import { Component } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {
	contato = {
		nome: '',
		email: '',
		telefone: ''
	}
  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http) {
  }

  cadastrarContato(){
  	let body = JSON.stringify(this.contato);
  	let headers = new Headers({'Content-Type':'application/json'});
  	let options = new RequestOptions({headers: headers});

  	this.http.post('http://192.68.0.125:3000/contatos', body, options)
  		.map(res => res.json())
  		.toPromise()
  		.then(response => {
        console.log(response);
        this.navCtrl.pop();
      });
  }
}
