import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-detalhe',
  templateUrl: 'detalhe.html',
})
export class DetalhePage {
	public contato;
  constructor(public navParams: NavParams) {
  	this.contato = navParams.get('contatoSelecionado');
  }
}