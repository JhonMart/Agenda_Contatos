import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavController, NavParams } from 'ionic-angular';
import {ContatoPage}  from '../contato/contato';
import { CadastroPage }  from '../cadastro/cadastro';
import { DetalhePage }  from '../detalhe/detalhe';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  contatos = [];
  public load;

  constructor(public navCtrl: NavController, public navParams: NavParams, private loader: LoadingController, private http : Http, private alertCtrl: AlertController) {
    this.load = this.loader.create({
      content: "Buscando contatos. Aguarde ...",
    });

    this.load.present();
  }

  ionViewWillEnter(){
    this.obterContatosAPI();
  }

  selecionaContato(x) {
    console.log(x.nome);
    this.navCtrl.push(ContatoPage, { contatoSelecionado : x } );
  }

  detalheContato(x) {
    console.log(x.nome);
    this.navCtrl.push(DetalhePage, { contatoSelecionado : x } );
  }

  adicionaContato(){
    console.log('Adicionando contato ...');
    this.navCtrl.push(CadastroPage);
  }

  obterContatosAPI() {
    this.http.get('http://192.168.0.125:3000/contatos')
        .map(response => response.json())
        .toPromise()
        .then(response => {
          this.contatos = response;
          this.load.dismiss();
        },
        err => {
          this.load.dismiss();
          this.alertCtrl.create({
            title: 'Falha na conexão!',
            buttons: [{text: 'Estou ciente'}],
            subTitle: 'Não foi possível obter a lista de contatos. Tente mais tarde.'
          }).present();
        })
  }

  apagarContato(x) {
    let urlJson = 'http://192.168.0.125:3000/contatos/'+x.id;
    this.http.delete(urlJson)
      .toPromise()
      .then(response => {
        console.log(response);
        this.navCtrl.push(HomePage);
      });
  }

  getItems(ev: any) {

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.contatos = this.contatos.filter((item) => {
        return (item.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }else{
      // Reset items back to all of the items
      this.obterContatosAPI();
    }
  }
}
