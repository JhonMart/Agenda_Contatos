import { Component } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-contato',
  templateUrl: 'contato.html'
})

export class ContatoPage {

  public contato;

  constructor(public navCtrl: NavController,public navParams: NavParams, private http: Http) {
    this.contato = navParams.get('contatoSelecionado');
  }

  atualizarContato() {
    let body = JSON.stringify(this.contato);
  	let headers = new Headers({'Content-Type':'application/json'});
  	let options = new RequestOptions({headers: headers});

  	this.http.put('http://192.68.0.125:3000/contatos/'+this.contato.id, body, options)
  		.map(res => res.json())
  		.toPromise()
  		.then(response => {
        console.log(response);
        this.navCtrl.pop();
      });
  }
}
